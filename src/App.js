import React from "react";
import Header from "./components/Header";
import Main from "./components/Main";
import Footer from "./components/Footer";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./components/Login";
import Register from "./components/sub-contents/Register";
import AboutUs from "./components/sub-contents/AboutUs";
import ContractUs from "./components/sub-contents/ContractUs";
import Faq from "./components/sub-contents/Faq";
import TermAndCondition from "./components/sub-contents/TermAndCondition";
import NotFound from "./components/NotFound";
import AddProjects from "./components/AddProjects";
import UserProfile from "./components/UserProfile";
import Rewards from "./components/sub-contents/Rewards";
import Payment from "./components/Payment";
import Projects from "./components/Projects";
import Airee from "./components/ProjectDetail/Airee";
import LatestProjectDetail from "./components/ProjectDetail/LatestProjectDetail";
const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/project-airee" element={<Airee />} exact />
          <Route path="/" element={<Main />} />
          <Route path="/login" element={<Login />} />
          <Route path="/projects" element={<Projects />} />
          <Route path="/register" element={<Register />} />
          <Route path="/aboutUs" element={<AboutUs />} />
          <Route path="/askUs" element={<Faq />} />
          <Route path="/contract" element={<ContractUs />} />
          <Route path="/termAndCondition" element={<TermAndCondition />} />
          <Route path="*" element={<NotFound />} />
          <Route path="/add-projects" element={<AddProjects />} />
          <Route path="/profile" element={<UserProfile />} />
          <Route path="/rewards" element={<Rewards />} />
          <Route path="/payment" element={<Payment />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
};

export default App;

import { library } from "@fortawesome/fontawesome-svg-core";
import { faFontAwesome, faBookmark } from "@fortawesome/free-regular-svg-icons";
import {
  faArrowRightLong,
  faBars,
  faMagnifyingGlass,
} from "@fortawesome/free-solid-svg-icons";
export const Font = library.add(
  faFontAwesome,
  faMagnifyingGlass,
  faBars,
  faArrowRightLong,
  faBookmark
);

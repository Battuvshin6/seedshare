import React from "react";
const Footer = () => {
  return (
    <div className="Footer">
      <div className="footer-logo">
        <a href="/home">
          <img src="/images/logo-black.svg" alt="logo img" />
          Seedshare
        </a>
      </div>
      <div className="footer-desc">
        <div className="footer-anchors">
          <p>About us :</p>
          <div className="footer-anchors-div">
            <a href="/about-us">Бидний тухай</a>
            <a href="/ask-us">Түгээмэл асуултууд</a>
            <a href="/contract">Холбоо барих</a>
            <a href="/term-and-condition">Үйлчилгээний нөхцөл</a>
          </div>
        </div>
        <div className="footer-contract">
          <p>Connect us :</p>
          <div className="footer-contract-anchors">
            <a href="https://www.facebook.com/seedshare.mn">Facebook</a>
            <a href="/#">Hello@seedshare.mn</a>
            <a href="https://www.instagram.com/seedsharemn/">Instagram</a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;

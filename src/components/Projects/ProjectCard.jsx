import React from "react";
import { Card } from "react-bootstrap";
import { HiArrowLongRight } from "react-icons/hi2";
const ProjectCard = (props) => {
  return (
    <Card style={{ width: "100%", marginRight: "0px" }}>
      <Card.Img variant="top" src="/images/art-project/art-img.svg" />
      <Card.Body>
        <Card.Title style={{ fontWeight: "bold" }}>{props.title}</Card.Title>
        <Card.Text>{props.desc}</Card.Text>
      </Card.Body>
      <Card.Footer style={{ backgroundColor: "white" }}>
        <div className="card-goal">
          <div className="card-goal-left">
            <p>
              Зорилт : <b>₮{props.goal}</b>
            </p>
            <div className="process">
              <div
                className="process-bar"
                style={{ width: props.percentage }}
              ></div>
            </div>
            <span className="process-money">₮{props.current}</span>
          </div>
          <div className="card-goal-right">
            <HiArrowLongRight size={30} />
          </div>
        </div>
      </Card.Footer>
    </Card>
  );
};
export default ProjectCard;

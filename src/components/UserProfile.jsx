import React from "react";
import { GrImage } from "react-icons/gr";

const UserProfile = () => {
  return (
    <div className="UserProfile">
      <div className="profile-imgs">
        <img
          src="/images/profile-bg.svg"
          alt="background"
          className="img-fluid background-img"
        />
        <img
          src="/images/user-profile.svg"
          alt="profile"
          className="profile-img"
        />
      </div>
      <form className="userprofile-form">
        <div className="profile-name">
          <label htmlFor="surname">
            <span>Овог нэр</span>
            <div className="user-profile-surname-input">
              <input
                type="text"
                name="surname"
                placeholder="Surname"
                required
              />
            </div>
          </label>
          <label htmlFor="firstname">
            <span>Өөрийн нэр</span>
            <div className="user-profile-firstname-input">
              <input
                type="text"
                name="firstname"
                placeholder="Firstname"
                required
              />
            </div>
          </label>
        </div>

        <div className="confirm-registration">
          <h5>Бүртгэлээ баталгаажуулах</h5>
          <p>1. Иргэний үнэмлэхний зураг оруулах</p>
        </div>

        <label htmlFor="id-card" className="id-card-label">
          <div className="id-card-div">
            <div className="id-card-front">
              <label
                htmlFor="id-card-front"
                className="id-card-front-icon-label"
              >
                <GrImage />
                <p className="upload-img-para">Зураг хуулах</p>
              </label>
              <input
                type="file"
                name="id-card-front"
                id="id-card-front"
                accept="image/*"
                required
              />
              <p className="id-card-front-para">Урд тал</p>
            </div>
            <div className="id-card-back">
              <label htmlFor="id-card-back" className="id-card-back-icon-label">
                <GrImage />
                <p className="upload-img-para">Зураг хуулах</p>
              </label>
              <input
                type="file"
                name=""
                accept="image/*"
                id="id-card-back"
                required
              />
              <p className="id-card-back-para">Ард тал</p>
            </div>
          </div>
        </label>
        <div className="reg-num-and-phone-num">
          <label htmlFor="registration">
            <span>Регистэрийн дугаар</span>
            <div className="user-profile-registration-input">
              <input type="text" name="registration" required />
            </div>
          </label>
          <label htmlFor="phone-num">
            <span>Утасны дугаар</span>
            <div className="user-profile-phone-num-input">
              <input type="tel" name="phone-num" required />
            </div>
          </label>
        </div>
        <button type="submit" className="register-btn primary-btn">
          Хадгалах
        </button>
      </form>
    </div>
  );
};

export default UserProfile;

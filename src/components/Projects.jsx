import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Font from "../FontAwesome/FontAwesome";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import TechnologyProjects from "./Projects/TechnologyProjects";
import ProjectCard from "./Projects/ProjectCard";
import GamesProjects from "./Projects/GamesProjects";
import {
  responsive,
  projectData,
  gameProjectData,
  techProjectData,
} from "./Projects/ProjectData";

const Projects = () => {
  const data = projectData.map((props) => {
    return (
      <ProjectCard
        key={props.id}
        title={props.title}
        desc={props.desc}
        goal={props.goal}
        current={props.current}
        percentage={props.barPercentage}
      />
    );
  });
  const gameData = gameProjectData.map((props) => {
    return (
      <GamesProjects
        key={props.id}
        title={props.title}
        desc={props.desc}
        goal={props.goal}
        current={props.current}
        percentage={props.barPercentage}
      />
    );
  });
  const techData = techProjectData.map((props) => {
    return (
      <TechnologyProjects
        img={props.projectImg}
        key={props.id}
        title={props.title}
        desc={props.desc}
        goal={props.goal}
        current={props.current}
        percentage={props.barPercentage}
      />
    );
  });
  return (
    <div className="Projects">
      <div className="projects-header">
        <FontAwesomeIcon icon="fa-solid fa-magnifying-glass" />
        <input type="search" placeholder="Search" />
      </div>
      <div className="art-project-carousel">
        <div className="artprojects-header">
          <p>Урлаг</p>
          <h2>
            Урлагийн бүтээлүүдэд <br /> хөрөнгө оруулах
          </h2>
        </div>
        <Carousel
          showDots={true}
          responsive={responsive}
          swipeable={false}
          draggable={true}
          ssr={true}
          keyBoardControl={true}
          customTransition="all .8"
          transitionDuration={700}
          containerClass="carousel-container"
          dotListClass="custom-dot-list-style"
        >
          {data}
        </Carousel>
      </div>
      <div className="tech-project-carousel">
        <div className="technologyProjects-header">
          <p>Технологи</p>
          <h2>
            Хамгийн шинэлэг <br /> технологууд
          </h2>
        </div>
        <Carousel
          showDots={true}
          responsive={responsive}
          swipeable={false}
          draggable={true}
          ssr={true}
          keyBoardControl={true}
          customTransition="all .8"
          transitionDuration={700}
          containerClass="carousel-container"
          dotListClass="custom-dot-list-style"
        >
          {techData}
        </Carousel>
      </div>

      <div className="game-project-carousel">
        <div className="gamesprojects-header">
          <p>Тоглоом</p>
          <h2>
            Сонирхолтой адал <br /> явдалт тоглоомууд
          </h2>
        </div>
        <Carousel
          showDots={true}
          responsive={responsive}
          swipeable={false}
          draggable={true}
          ssr={true}
          keyBoardControl={true}
          customTransition="all .8"
          transitionDuration={700}
          containerClass="carousel-container"
          dotListClass="custom-dot-list-style"
        >
          {gameData}
        </Carousel>
      </div>
    </div>
  );
};

export default Projects;

import React from "react";
import styled from "styled-components";
const Body = styled.div`
  display: flex;
  flex-direction: row;

  margin: 50px 0px;
`;
const SmallRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 10px;
`;
const BigRow = styled.div`
  display: none;
  @media screen and (min-width: 992px) {
    display: flex;
  }
`;
const ContentImg = styled.img`
  width: 50%;
  height: 100%;
  border-radius: 7px;
`;
const ContentDesc = styled.div``;
const ContentHeader = styled.h5`
  font-size: 1rem;
`;
const ContentDetail = styled.p`
  font-size: 0.6rem;
  font-weight: 600;
`;
const ProcessContainer = styled.div`
  display: flex;
  flex-direction: column;
`;
const Process = styled.div`
  display: flex;
  background-color: gainsboro;
  height: 0.4rem;
  border-radius: 25px;
`;
const ProcessBar = styled.div`
  background-color: #13ce66;
  height: 0.4rem;
  border: 1px solid #13ce66;
  border-radius: 25px;
  margin-left: 2px;
`;
const GoalSpan = styled.span`
  font-size: 0.5rem;
  font-weight: 600;
  @media screen and (min-width: 600px) {
    font-size: 0.8rem;
  }
  @media screen and (min-width: 1200px) {
    font-size: 1rem;
  }
`;
const ValuePercentage = styled.p`
  color: #13ce66;
  font-weight: 700;
  margin: 0;
`;
const GoalContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 3px;
`;

const LatestProjectDetail = (props) => {
  return (
    <Body>
      <SmallRow>
        <ContentImg src="/images/ger.png" />
        <ContentDesc>
          <ContentHeader>{props.title}</ContentHeader>
          <ContentDetail>{props.desc}</ContentDetail>
          <ProcessContainer>
            <Process>
              <ProcessBar
                style={{ width: props.percentage + "%" }}
              ></ProcessBar>
            </Process>
            <GoalContainer>
              <ValuePercentage>{props.percentage}%</ValuePercentage>
              <GoalSpan>Төслийн зорилго: ₮{props.goal}</GoalSpan>
            </GoalContainer>
          </ProcessContainer>
        </ContentDesc>
      </SmallRow>

      <BigRow>
        <ContentImg src="/images/ger.png" />
        <ContentDesc>
          <ContentHeader>{props.title}</ContentHeader>
          <ContentDetail>{props.desc}</ContentDetail>
          <ProcessContainer>
            <Process>
              <ProcessBar
                style={{ width: props.percentage + "%" }}
              ></ProcessBar>
            </Process>
            <GoalContainer>
              <ValuePercentage>{props.percentage + "%"}</ValuePercentage>
              <GoalSpan>Төслийн зорилго: ₮{props.goal}</GoalSpan>
            </GoalContainer>
          </ProcessContainer>
        </ContentDesc>
      </BigRow>
    </Body>
  );
};
export default LatestProjectDetail;

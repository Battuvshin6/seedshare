import React from "react";
import { techProjectData } from "../Projects/ProjectData";
import { RWebShare } from "react-web-share";
import { HiOutlineShare } from "react-icons/hi";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Font } from "../../FontAwesome/FontAwesome";
import styled from "styled-components";

const GoalSpan = styled.span`
  font-size: 1rem;
  font-weight: 500;
  color: rgba(0, 0, 0, 0.7);
`;
const AuthorContainer = styled.div`
  display: flex;
  align-items: center;
  gap: 0.3rem;
  margin-bottom: 10px;
`;
const ProcessDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 90%;
`;
const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 50px 20px;
`;
const Header = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  margin-bottom: 1rem;
  @media screen and (min-width: 600px) {
    width: 90%;
  }
`;
const AireeHeaderH2 = styled.h2`
  font-weight: 700;
  font-size: 1.2rem;
  margin-bottom: 2rem;
  @media screen and (min-width: 600px) {
    font-size: 1.5rem;
  }
`;
const Process = styled.div`
  display: flex;
  align-items: center;
  background-color: gainsboro;
  height: 0.7rem;
  border-radius: 25px;
`;
const ProcessBar = styled.div`
  background-color: #13ce66;
  height: 0.5rem;
  border: 1px solid #13ce66;
  border-radius: 25px;
  margin-left: 2px;
`;
const CurrentValue = styled.p`
  font-size: 1.5rem;
  font-weight: 700;
`;
const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  gap: 12px;
  border: 1px solid #f8f8f8;
  border-radius: 10px;
  padding: 10px;
  width: 100%;
`;
const SaveShare = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 12px;
  border: 1px solid #f8f8f8;
  border-radius: 10px;
  padding: 13px;
  height: 100%;
`;
const BtnContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-evenly;
  margin-bottom: 1rem;
  gap: 0.6rem;
  @media screen and (min-width: 600px) {
    width: 100%;
  }
`;
const Fontfixer = styled.span`
  text-align: center;
  font-size: 0.5rem;
  font-weight: 500;
  @media screen and (min-width: 600px) {
    font-size: 1rem;
  }
`;
const NumFont = styled.span`
  font-size: 1rem;
  font-weight: 700;
  @media screen and (min-width: 600px) {
    font-size: 1.5rem;
  }
`;
const NavBtn = styled.button`
  margin-bottom: 2rem;
  background-color: #13ce66;
  font-weight: 700;
  border: 1px solid #13ce66;
  border-radius: 25px;
  padding: 10px 20px;
  display: flex;
  align-items: center;
  gap: 10px;
  @media screen and (min-width: 600px) {
    font-size: 1.4rem;
    padding: 13px 26px;
  }
`;
const ContentImg = styled.img`
  width: 100%;
  height: auto;
  @media screen and (min-width: 600px) {
    width: 100%;
  }
`;

const Body = styled.div`
  @media screen and (min-width: 600px) {
    width: 90%;
  }
`;
const ProfileImg = styled.img`
  border-radius: 50px;
  height: 30px;
  width: 30px;
  @media screen and (min-width: 600px) {
    height: 40px;
    width: 40px;
  }
`;
const AuhorName = styled.span`
  font-weight: 600;
  @media screen and (min-width: 600px) {
    font-size: 1.2rem;
  }
`;
const Description = styled.p`
  font-size: 1.2rem;
`;
const Airee = (props) => {
  const data = techProjectData;
  const title = data.find((item) => item.id === 1);
  const SaveHander = () => {
    return alert("sorry this feature is not complete yet");
  };
  return (
    <Container>
      <Header className="airee-header">
        <AireeHeaderH2>{title.title}</AireeHeaderH2>
        <ContentImg src="/images/airee.png" alt="airee" />
      </Header>
      <ProcessDiv>
        <AuthorContainer>
          <ProfileImg src="/images/joker.jpeg" alt="pic" />
          <AuhorName>Kaze</AuhorName>
        </AuthorContainer>
        <GoalSpan>Төслийн зорилго ₮{title.goal}</GoalSpan>
        <Process>
          <ProcessBar style={{ width: title.barPercentage }}></ProcessBar>
        </Process>
        <CurrentValue>₮{title.current}</CurrentValue>
      </ProcessDiv>
      <BtnContainer>
        <FlexRow>
          <Fontfixer>Үлдсэн өдөр</Fontfixer>
          <NumFont>16</NumFont>
        </FlexRow>
        <FlexRow>
          <Fontfixer>Дэмжигчид</Fontfixer>
          <NumFont>86</NumFont>
        </FlexRow>
        <SaveShare>
          <RWebShare
            onClick={() => alert("shared sucessfully")}
            data={{
              text: "What",
              url: "https://www.youtube.com/",
              title: "lemme see",
            }}
          >
            <HiOutlineShare className="save" />
          </RWebShare>
        </SaveShare>
        <SaveShare onClick={SaveHander}>
          <FontAwesomeIcon icon="fa-regular fa-bookmark" className="share" />
        </SaveShare>
      </BtnContainer>
      <NavBtn type="submit">
        Төслийг дэмжих <FontAwesomeIcon icon="fa-solid fa-arrow-right-long" />
      </NavBtn>
      <Body>
        <Description>
          Дэлхийн нийт хүчилтөрөгчийн 40-70 хувийг далайн замаг ялгаруулдаг.
          Гэвч бидний өргөн хэрэглэж буй агаар цэвэршүүлэгчийн шүүлтүүр нь
          синтетик материалаар хийгддэг тул эргээд байгальд задрахдаа микро
          пластик болдог. Тэгэхээр өнөөдөр цэвэр агаар амьсгалахын тулд хийж буй
          сонголт маань ирээдүйд хүчилтөрөгч ялгаруулах далайн замгийг боомилох
          хог хаягдал болж байна гэсэн үг.
        </Description>
        <ContentImg src="/images/airee2.png" alt="airee2" />
      </Body>
    </Container>
  );
};

export default Airee;

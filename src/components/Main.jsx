import React from "react";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";
import Weekly from "./sub-contents/Weekly";
import Latest from "./sub-contents/Latest";
import { useNavigate } from "react-router-dom";
const Main = () => {
  const nav = useNavigate();
  const navigation = () => {
    nav({ pathname: "/add-projects" });
  };
  return (
    <div className="Main">
      <div className="container-fluid">
        <div className="row main-row">
          <div className="col-sm-12 col-lg-5 col-md-5 main-left-part">
            <h1>
              Төсөөллөө бодит болгох боломжоор <br /> дүүрэн талбарт тавтай
              морил
            </h1>
            <img
              src="/images/green-step.svg"
              alt="green-img"
              className="green-step"
            />
            <button type="button" className="dream-btn" onClick={navigation}>
              <span>
                Мөрөөдлөө бодит болгох
                <HiOutlineArrowNarrowRight />
              </span>
            </button>
            <button className="main-add-project-btn" type="button">
              <a href="/add-projects">
                <img src="/images/btn-img.svg" alt="button" />
                <span>Төсөл байршуулах</span>
              </a>
            </button>
          </div>
        </div>
      </div>
      <div className="sub-contents">
        <div className="weekly-projects">
          <Weekly />
        </div>
        <div className="latest-projects">
          <Latest />
        </div>
      </div>
    </div>
  );
};

export default Main;

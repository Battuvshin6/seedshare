import React from "react";

const Payment = () => {
  const data = {
    name: "Kaze",
    payment: 15000,
  };
  return (
    <div className="Payment">
      <div className="payment-header">
        <p>Таны нийт төлөх дүн</p>
        <h4>{data.payment} ₮</h4>
      </div>
      <div className="payment-body">
        <div className="top-row">
          <button>
            <img src="/images/khaanbank.png" alt="bank" />
            <span>Хаан Банк</span>
          </button>
          <button>
            <img src="/images/tdb.png" alt="bank" />
            <span>ХХБ</span>
          </button>
          <button>
            <img src="/images/golomt.jpeg" alt="golomt" />
            <span>Голомт банк</span>
          </button>
        </div>
        <div className="bottom-row">
          <button>
            <img src="/images/has.png" alt="has" />
            <span>Хас Банк</span>
          </button>
          <button>
            <img src="/images/most-money.png" alt="most-money" />
          </button>
          <button>
            <img src="/images/paypal.png" alt="paypal" />
          </button>
        </div>
      </div>
      <div className="payment-footer">
        <img src="/images/WarningCircle.png" alt="warning" />
        <p>
          Банкны гадаад валютын дансаар тус гүйлгээг хийхгүй байхыг анхаарна уу.
        </p>
        <p>
          PayPal данс ашиглаж гүйлгээ хийхэд танд гүйлгээний шимтгэл нэмж
          тооцогдож болохыг анхаарна уу.
        </p>
      </div>
    </div>
  );
};

export default Payment;

import React from "react";

export default function NotFound() {
  return (
    <div className="notFound">
      <img src="/images/error-404.png" alt="error" className="img-fluid" />
    </div>
  );
}

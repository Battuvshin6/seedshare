import React from "react";
import { MdOutlineEmail } from "react-icons/md";
import { TfiLock } from "react-icons/tfi";
import { BsGoogle, BsXLg } from "react-icons/bs";
import { FaFacebookF } from "react-icons/fa";
import { Icon } from "react-icons-kit";
import { eye } from "react-icons-kit/feather/eye";
import { eyeOff } from "react-icons-kit/feather/eyeOff";
import { useState } from "react";
const Login = () => {
  const [type, setType] = useState("password");
  const [icon, setIcon] = useState(eyeOff);
  const [message, setMessage] = useState("");

  // Cleaning input Field
  const HandleChange = (event) => {
    setMessage(event.target.value);
  };
  const HandleClick = () => {
    setMessage("");
  };
  // Toggling password hider Icon
  const handleToggle = () => {
    if (type === "password") {
      setIcon(eye);
      setType("text");
    } else {
      setIcon(eyeOff);
      setType("password");
    }
  };

  return (
    <div className="Login">
      <div className="row login-row">
        <div className="col-0 col-md-6 col-lg-6  col-xl-5 login-left-col">
          <div className="login-header">
            <h3>
              Хүсэл мөрөөдлөө <br /> бусадтай хуваалц
            </h3>
            <img src="/images/green-step.svg" alt="green-img" />
          </div>
          <div className="login-btns">
            <div className="welcome-btn">
              <button type="button">
                <img src="/images/wave-emoji.svg" alt="wave-emoji" />
              </button>
              <div className="welcome-btn-imgs">
                <img src="/images/profile-pic.svg" alt="profile-img" />
                <img src="/images/profile-pic.svg" alt="profile-img" />
                <img src="/images/profile-pic.svg" alt="profile-img" />
              </div>
            </div>
            <div className="three-dot">
              <button type="submit">
                <img src="/images/3dot.svg" alt="..." />
              </button>
            </div>
          </div>

          <div className="login-footer">
            <p>
              Бүртгэлгүй хэрэглэгч бол <a href="/register">Бүртгүүлэх</a>
            </p>
          </div>
        </div>
        <div
          className="col-12 col-md-12 col-lg-6 col-xl-7 login-right-col"
          id="login-right-col"
        >
          <form action="" className="login-form">
            <h2>Нэвтрэх</h2>
            <label htmlFor="email">
              <span className="login-label-span">Цахим хаяг</span>
              <div className="login-email-div">
                <MdOutlineEmail size={25} color={"white"} />
                <input
                  type="email"
                  name="email"
                  placeholder="Email or username"
                  className="login-email-input"
                  value={message}
                  onChange={HandleChange}
                />

                <span>
                  <BsXLg color={"white"} onClick={HandleClick} />
                </span>
              </div>
            </label>
            <label htmlFor="password" className="login-password-label">
              <span className="login-label-span">Нууц үг</span>
              <div className="login-password-div">
                <span>
                  <TfiLock size={25} />
                  <input
                    type={type}
                    name="password"
                    placeholder="Нууц үг"
                    className="login-password-input"
                    id="login-password-input"
                  />
                </span>
                <span className="password-toggle-icon" onClick={handleToggle}>
                  <Icon icon={icon} size={20} />
                </span>
              </div>
            </label>
            <div className="login-remember-me">
              <input
                type="checkbox"
                name="checkbox"
                id=""
                className="remember-tick"
              />
              <span>Намайг cанах</span>
            </div>
            <button type="submit" className="login-btn">
              Нэвтрэх
            </button>
            <a href="/forgot-password" className="forgotten-password">
              Нууц үгээ мартсан уу?
            </a>
          </form>
          <div className="social-media-login">
            <p>Эсвэл эдгээрээр нэвтрэх</p>
            <div className="social-media-login-btn">
              <button type="button">
                <span>
                  <BsGoogle size={15} />
                  <p>Google Account</p>
                </span>
              </button>
              <button type="button">
                <span>
                  <FaFacebookF size={15} />
                  <p className="fa-facebook-icon"> Facebook</p>
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;

import React from "react";
import { Question, Answers } from "../../data/Faq";

import { useState } from "react";
const Faq = () => {
  const [show, setShow] = useState();
  return (
    <div className="Faq">
      <div className="faq-header">
        <h1>FAQS</h1>
        <img src="/images/green-step.svg" alt="contnt" />
        <span>Бидэнд ирдэг түгээмэл асуултууд</span>
      </div>
      <div className="faq-content">
        <div className="faq-questions">
          <p>1.Crowdfunding гэж юу вэ?</p>
          <button>Дэлгэрэнгүй</button>
        </div>
        {show && <div className="faq-answers"></div>}
      </div>
    </div>
  );
};

export default Faq;

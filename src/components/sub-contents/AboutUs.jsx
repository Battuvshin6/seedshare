import React from "react";
<<<<<<< HEAD

const AboutUs = () => {
  return (
    <div className="AboutUs">
      <div className="about-us-header">
        <h1>
          Seedshare <br /> таньд юу өгч чадах вэ?
        </h1>
        <img src="/images/green-step.svg" alt="img" className="img-fluid" />
        <span>
          Итгэл найдвар, үнэнч хэрэглэгч, <br /> бас чанар.
        </span>
      </div>
      <section className="about-us-article-section">
        <img src="/images/grandma.svg" alt="" className="img-fluid" />
        <h3>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis,
          temporibus?
        </h3>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Earum
          doloremque numquam exercitationem porro corporis architecto.
          Repellendus, enim itaque? Cumque, ab? Lorem ipsum dolor sit amet,
          consectetur adipisicing elit. Suscipit, voluptates! Lorem ipsum dolor
          sit amet, consectetur adipisicing elit. Labore asperiores esse a
          libero, amet perspiciatis!
        </p>
      </section>
      <article className="about-us-article">
        <div className="row">
          <div className="col about-us-first-col-desc">
            <span>2022</span>
            <h3>Why do we use it?</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Temporibus in earum sunt delectus et fugit, nemo laudantium.
              Maiores neque, sunt libero maxime quae reprehenderit adipisci
              asperiores rem quidem laudantium, quaerat cumque voluptatem
              doloribus minus iusto eum vero quibusdam excepturi ipsum ad
              temporibus iure nisi distinctio! Harum eligendi quaerat assumenda
              voluptas! Lorem ipsum, dolor sit amet consectetur adipisicing
              elit. Cupiditate laudantium dignissimos architecto aliquam
              corrupti distinctio quis recusandae repellendus autem consequatur
              temporibus
            </p>
          </div>
          <div className="col">
            <img
              src="/images/article-img.svg"
              alt="content"
              className="img-fluid"
            />
          </div>
        </div>
        <div className="gap-div"></div>
        <div className="row">
          <div className="col">
            <img
              src="/images/article-img.svg"
              alt="content"
              className="img-fluid"
            />
          </div>
          <div className="col about-us-second-col-desc">
            <span>2022</span>
            <h3>Why do we use it?</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Temporibus in earum sunt delectus et fugit, nemo laudantium.
              Maiores neque, sunt libero maxime quae reprehenderit adipisci
              asperiores rem quidem laudantium, quaerat cumque voluptatem
              doloribus minus iusto eum vero quibusdam excepturi ipsum ad
              temporibus iure nisi distinctio! Harum eligendi quaerat assumenda
              voluptas! Lorem ipsum dolor sit amet consectetur adipisicing elit.
              Vel, beatae? Lorem ipsum dolor sit, amet consectetur adipisicing
              elit. Id, inventore? Lorem ipsum dolor, sit amet consectetur
              adipisicing elit. Provident expedita omnis labore eos earum quasi
              laborum explicabo a consequatur veniam!
            </p>
          </div>
        </div>
      </article>
    </div>
=======
import styled from "styled-components";
const Container = styled.div`
  margin: 50px 20px;
  @media screen and (min-width: 600px) {
    margin: 50px 3rem;
  }
  @media screen and (min-width: 992px) {
    margin: 50px 0;
  }
`;
const BackgroungImg = styled.img`
  max-width: 100%;
  height: auto;
  display: none;
  @media screen and (min-width: 992px) {
    display: block;
  }
`;
const Description = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  @media screen and (min-width: 992px) {
    margin: 50px 120px;
  }
`;

const Header = styled.h1`
  font-weight: 700;
  font-size: 1.4rem;
  margin-bottom: 5px;
  @media screen and (min-width: 768px) {
    font-size: 1.8rem;
  }
  @media screen and (min-width: 992px) {
    font-size: 2.2rem;
  }
`;
const Body = styled.div`
  display: none;
  @media screen and (min-width: 992px) {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    gap: 50px;
    margin: 50px 0;
  }
  @media screen and (min-width: 1200px) {
    gap: 80px;
  }
`;
const Content = styled.div`
  display: flex;
  flex-direction: row;
  gap: 20px;
`;

const ContentImg = styled.img`
  width: 50%;
  height: 100%;
  border-radius: 5px;
`;
const ContentDetail = styled.div``;
const ContentHeader = styled.h3`
  font-weight: 700;
  margin-top: 20px;
  @media screen and (min-width: 1200px) {
    font-size: 2rem;
  }
`;
const ContentDesc = styled.p`
  font-size: 0.9rem;
  font-weight: 600;
  @media screen and (min-width: 1200px) {
    font-size: 1.1rem;
  }
`;
const HeaderStep = styled.img`
  width: 80%;
  margin-bottom: 20px;
  @media screen and (min-width: 600px) {
    width: 40%;
  }
  @media screen and (min-width: 768px) {
    width: 30%;
  }
  @media screen and (min-width: 992px) {
    width: 40%;
  }
  @media screen and (min-width: 1200px) {
    width: 25%;
  }
`;
const BodySmall = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  @media screen and (min-width: 992px) {
    display: none;
  }
`;
const ContentSmall = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const SmallImg = styled.img`
  width: 100%;
  height: auto;
  @media screen and (min-width: 768px) {
    width: 80%;
  }
`;
const SmallDetail = styled.div`
  margin: 20px;
  @media screen and (min-width: 768px) {
    width: 70%;
  }
`;
const SmallHeader = styled.h1`
  font-size: 1rem;
  font-weight: 700;
  @media screen and (min-width: 768px) {
    font-size: 1.2rem;
  }
`;
const SmallDesc = styled.p`
  font-size: 0.8rem;
  font-weight: 600;
  @media screen and (min-width: 768px) {
    font-size: 1rem;
  }
`;
const UnorderedList = styled.ul`
  list-style: none;
  padding: 0;
`;
const List = styled.li`
  font-weight: 700;
`;
const SpecialImg = styled.img`
  width: 50%;
  height: 292px;
  border-radius: 5px;
`;
const AboutUs = () => {
  return (
    <Container>
      <BackgroungImg src="/images/aboutUs.png" />
      <Description>
        <Header>Seedshare гэж юу вэ?</Header>
        <HeaderStep src="/images/green-step.svg" />
        <BodySmall>
          <ContentSmall>
            <SmallImg src="/images/mountain.png" />
            <SmallDetail>
              <SmallHeader>БИДНИЙ ЗОРИЛГО </SmallHeader>
              <SmallDesc>
                Seedshare нь төсөөллөө бодит болгохоор зорьж буй бүтээгч, бизнес
                эрхлэгчдэд гарааны санхүүжилтын асуудлаа шийдэхэд нь туслах
                зорилготой crowdfunding платформ юм.
              </SmallDesc>
            </SmallDetail>
          </ContentSmall>
          <ContentSmall>
            <SmallImg src="/images/mountain.png" />
            <SmallDetail>
              <SmallHeader>ХЭРХЭН АЖИЛЛАДАГ ВЭ?</SmallHeader>
              <SmallDesc>
                Crowdfunding нь олон нийтээс мөнгө татан төвлөрүүлж, төсөлд
                санхүүжилт босгож өгөх зарчмаар ажилладаг бөгөөд манай платформ
                нь урамшуулал дээр суурилсан Монголын анхны crowdfunding юм.
                Өөрөөр хэлбэл та төслөө манай платформ дээр байршуулан олон
                нийтээс санхүүжилт босгосон бол төсөлд тань мөнгөн дэмжлэг
                үзүүлсэн хүмүүстээ та эргэн төслийнхөө үр шимийг буюу урамшуулал
                өгөх юм.
              </SmallDesc>
            </SmallDetail>
          </ContentSmall>
          <ContentSmall>
            <SmallImg src="/images/mountain.png" />
            <SmallDetail>
              <SmallHeader>ЯАГАААД SEEDSHARE ГЭЖ?</SmallHeader>
              <SmallDesc>
                Seedshare бол таны төсөөллөө бодит болгох боломжоор дүүрэн
                талбар юм. Учир нь,
                <UnorderedList>
                  <List>-Ашиглахад хялбар</List>
                  <List>
                    -Хүнд сурталд дахин энерги, эрч хүчээ зарцуулахгүй
                  </List>
                  <List>-Цаг хугацаа хэмнэнэ</List>
                  <List>-Төслөө олон нийтэд таниулах боломж</List>
                </UnorderedList>
              </SmallDesc>
            </SmallDetail>
          </ContentSmall>
        </BodySmall>

        {/* ///////////////Display on 992 px ////////////// */}

        <Body>
          <Content>
            <ContentDetail>
              <ContentHeader>Бидний зорилго</ContentHeader>
              <ContentDesc>
                Seedshare нь төсөөллөө бодит болгохоор зорьж буй бүтээгч, бизнес
                эрхлэгчдэд гарааны санхүүжилтын асуудлаа шийдэхэд нь туслах
                зорилготой crowdfunding платформ юм.
              </ContentDesc>
            </ContentDetail>
            <SpecialImg src="/images/mountain.png" />
          </Content>
          <Content>
            <ContentImg src="/images/compass.png" />
            <ContentDetail>
              <ContentHeader>Хэрхэн ажилладаг вэ?</ContentHeader>
              <ContentDesc>
                Crowdfunding нь олон нийтээс мөнгө татан төвлөрүүлж, төсөлд
                санхүүжилт босгож өгөх зарчмаар ажилладаг бөгөөд манай платформ
                нь урамшуулал дээр суурилсан Монголын анхны crowdfunding юм.
                Өөрөөр хэлбэл та төслөө манай платформ дээр байршуулан олон
                нийтээс санхүүжилт босгосон бол төсөлд тань мөнгөн дэмжлэг
                үзүүлсэн хүмүүстээ та эргэн төслийнхөө үр шимийг буюу урамшуулал
                өгөх юм.
              </ContentDesc>
            </ContentDetail>
          </Content>
          <Content>
            <ContentDetail>
              <ContentHeader>Яагаад Seedshare гэж?</ContentHeader>
              <ContentDesc>
                Seedshare бол таны төсөөллөө бодит болгох боломжоор дүүрэн
                талбар юм. Учир нь,
                <UnorderedList>
                  <List>-Ашиглахад хялбар</List>
                  <List>
                    -Хүнд сурталд дахин энерги, эрч хүчээ зарцуулахгүй
                  </List>
                  <List>-Цаг хугацаа хэмнэнэ</List>
                  <List>-Төслөө олон нийтэд таниулах боломж</List>
                </UnorderedList>
              </ContentDesc>
            </ContentDetail>
            <ContentImg src="/images/cube.png" />
          </Content>
        </Body>
      </Description>
    </Container>
>>>>>>> sub
  );
};

export default AboutUs;

import React from "react";
import { HiArrowLongRight } from "react-icons/hi2";
import localData from "../../data/dummy.json";
import styled from "styled-components";

////////////////////////// STYLE /////////////////////////////

const Container = styled.div`
  overflow: hidden;
`;
const Header = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  margin-bottom: 15px;
  @media screen and (min-width: 992px) {
    margin: 5rem;
  }
`;
const HeaderPara = styled.p`
  font-weight: 700;
  margin: 0;
  @media screen and (min-width: 600px) {
    font-size: 1.4rem;
  }
  @media screen and (min-width: 992px) {
    font-size: 1.7rem;
  }
`;
const HeaderH2 = styled.h2`
  font-weight: 800;
  text-align: center;
  @media screen and (min-width: 600px) {
    font-size: 2.5rem;
  }
  @media screen and (min-width: 992px) {
    font-size: 3.5rem;
  }
`;
const HeaderSpan = styled.span`
  display: none;
  @media screen and (min-width: 992px) {
    display: flex;
    font-size: 1.5rem;
  }
`;
const Body = styled.div`
  margin: 0 30px 50px 30px;
  display: flex;
  flex-direction: column;
  @media screen and (min-width: 992px) {
    flex-direction: row;
    justify-content: center;
    margin: 0 30px 80px 30px;
  }
`;
const Description = styled.div`
  background-color: white;
  padding: 20px;
  @media screen and (min-width: 992px) {
    width: 40%;
  }
`;
const ImgContainer = styled.div`
  @media screen and (min-width: 992px) {
    width: 40%;
  }
`;
const Date = styled.span`
  font-size: 0.8rem;
  @media screen and (min-width: 600px) {
    font-size: 1rem;
  }
  @media screen and (min-width: 992px) {
    font-size: 1.2rem;
  }
`;
const Title = styled.h3`
  font-size: 1.2rem;
  font-weight: 700;
  @media screen and (min-width: 600px) {
    font-size: 1.5rem;
  }
  @media screen and (min-width: 992px) {
    font-size: 1.8rem;
  }
`;
const Detail = styled.p`
  font-size: 0.8rem;
  @media screen and (min-width: 600px) {
    font-size: 1rem;
  }
  @media screen and (min-width: 992px) {
    margin-bottom: 50px;
  }
`;
const GoalDiv = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 10px;
  align-items: center;
  @media screen and (min-width: 600px) {
    margin-top: 30px;
  }
  @media screen and (min-width: 992px) {
    margin-top: 50px;
  }
`;
const ProcessContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media screen and (min-width: 600px) {
    width: 40%;
    text-align: center;
    justify-content: center;
  }
  @media screen and (min-width: 992px) {
    width: 50%;
    text-align: start;
  }
  @media screen and (min-width: 1200px) {
    margin-left: 1px;
  }
`;
const Process = styled.div`
  display: flex;
  align-items: center;
  background-color: gainsboro;
  height: 0.4rem;
  border-radius: 25px;
  @media screen and (min-width: 600px) {
    height: 0.8rem;
  }
  @media screen and (min-width: 992px) {
    height: 0.8rem;
  }
`;
const ProcessBar = styled.div`
  background-color: #13ce66;
  height: 0.4rem;
  border: 1px solid #13ce66;
  border-radius: 25px;
  margin-left: 2px;
  @media screen and (min-width: 600px) {
    height: 0.8rem;

    @media screen and (min-width: 992px) {
      height: 0.8rem;
    }
  }
`;
const CurrentValue = styled.p`
  font-size: 1.2rem;
  font-weight: 700;
  margin: 0;
  @media screen and (min-width: 600px) {
    font-size: 1.5rem;
  }
  @media screen and (min-width: 1200px) {
    font-size: 2rem;
  }
`;
const GoalSpan = styled.span`
  font-size: 0.5rem;
  @media screen and (min-width: 600px) {
    font-size: 0.8rem;
  }
  @media screen and (min-width: 1200px) {
    font-size: 1rem;
  }
`;
const MoreBtn = styled.button`
  display: flex;
  align-items: center;
  gap: 5px;
  background-color: white;
  border: 1px solid rgba(0, 0, 0, 0.08);
  border-radius: 25px;
  font-size: 0.8rem;
  padding: 5px 10px;
  font-weight: 700;
  @media screen and (min-width: 600px) {
    font-size: 1rem;
    padding: 10px 20px;
  }
  @media screen and (min-width: 1200px) {
    font-size: 1.2rem;
    margin-right: 25px;
  }
`;
const ContentImg = styled.img`
  width: 100%;
  height: auto;
  @media screen and (min-width: 992px) {
    height: 500px;
  }
`;

////////////////////////// COMPONENT /////////////////////////////

const Weekly = () => {
  ////////////////////////// FUNCTIONS AND DATAS /////////////////////////////
  const data = localData.Argal;
  const screen = window.screen.width;
  const detect = 992;

  ////////////////////////// RENDER /////////////////////////////

  return (
    <Container>
      <Header>
        <HeaderPara>Энэ 7 хоногийн</HeaderPara>
        <HeaderH2>
          ОНЦЛОХ <br /> ТӨСӨЛ
        </HeaderH2>
        <HeaderSpan>Бид 7 хоног бүр шинэ төслийг онцлох болно.</HeaderSpan>
      </Header>
      <Body>
        <ImgContainer>
          {screen < detect ? (
            <ContentImg
              src="/images/argalsmall.png"
              className="img-fluid argal"
              alt="argal"
            />
          ) : (
            <ContentImg
              src="/images/argal.png"
              alt="argal"
              className="img-fluid argal"
            />
          )}
        </ImgContainer>
        <Description>
          <Date>{data.projectDate}</Date>
          <Title className="weekly-title">{data.projectTitle}</Title>
          {screen < detect ? (
            <Detail>
              Аргалын цаас нь дотоодын сэргээгдэх түүхий эд болох малын өтөг
              бууцаар цаас үйлдвэрлэхээр зорьж буй төсөл юм.
            </Detail>
          ) : (
            <Detail>{data.projectDetail}</Detail>
          )}
          <GoalDiv>
            <ProcessContainer>
              <Process>
                <ProcessBar
                  style={{ width: data.processPercentage + "%" }}
                ></ProcessBar>
              </Process>
              <CurrentValue>₮ {data.projectValue}</CurrentValue>
              <GoalSpan>Төслийн зорилго: ₮{data.projectGoal}</GoalSpan>
            </ProcessContainer>
            <MoreBtn>
              Дэлгэрэнгүй <HiArrowLongRight />
            </MoreBtn>
          </GoalDiv>
        </Description>
      </Body>
    </Container>
  );
};

export default Weekly;

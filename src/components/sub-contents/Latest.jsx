import React from "react";
import { HiOutlineArrowNarrowRight } from "react-icons/hi";
<<<<<<< HEAD
import { LatestData } from "../ProjectDetail/LatestData";
import LatestProjectDetail from "../ProjectDetail/LatestProjectDetail";
import styled from "styled-components";
const Container = styled.div`
  margin: 20px 20px;
  padding: 50px 0;
  @media screen and (min-width: 600px) {
    margin: 4rem 3rem;
  }
  @media screen and (min-width: 768px) {
    margin: 5rem 10rem;
  }
  @media screen and (min-width: 1200px) {
    margin: 5rem 25rem;
  }
`;
const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-bottom: 50px;
`;
const HeaderPara = styled.p`
  font-weight: 500;
  font-size: 15px;
  background-image: url(../public/images/green-border.svg);
  background-repeat: no-repeat;
  padding: 10px;
  background-size: contain;
  margin: 0;
  @media screen and (min-width: 600px) {
    font-size: 1.5rem;
    padding: 22px;
  }
`;
const HeaderH1 = styled.h1`
  font-size: 1.9rem;
  font-weight: 800;
  text-align: center;
  @media screen and (min-width: 600px) {
    font-size: 2.5rem;
  }
`;
const HeaderSpan = styled.span`
  text-align: center;
  @media screen and (min-width: 600px) {
    font-size: 1.2rem;
  }
  @media screen and (min-width: 768px) {
    font-size: 1.3rem;
  }
`;
const Footer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const Navigate = styled.a`
  text-decoration: none;
  color: black;
  border-bottom: 3px solid #13ce66;
  padding-bottom: 3px;
  font-weight: 500;
`;
=======
import Oortsog from "../../data/dummy.json";
import { ProgressBar } from "react-bootstrap";
>>>>>>> parent of 90c0cb2 (lates revamped)
const Latest = () => {
  const data = Oortsog.Oortsog;
  return (
<<<<<<< HEAD
    <Container>
      <Header>
        <HeaderPara>Шинэ</HeaderPara>
        <HeaderH1>Хамгийн сүүлийн</HeaderH1>
        <HeaderSpan>Таньд урам зориг өгөх төслүүд</HeaderSpan>
      </Header>
      <div className="weekly-body">{data}</div>
      <Footer>
        <Navigate href="/projects">
          <span>Бүх нийтлэгдсэн төслийг харах</span>
          <HiOutlineArrowNarrowRight />
        </Navigate>
      </Footer>
    </Container>
=======
    <div className="Latest">
      <div className="latest-project-header">
        <p>Шинэ</p>
        <h1>Хамгийн сүүлийн</h1>
        <span>Таньд урам зориг өгөх төслүүд</span>
      </div>
      <div className="latest-project-body">
        <div className="latest-project-show-on-sm">
          <div className="row">
            <div className="col-6 latest-project-left">
              <img
                src={data.projectImg}
                alt="project-img"
                className="img-fluid"
              />
            </div>
            <div className="col-6 latest-project-right-author">
              <h5>{data.projectTitle}</h5>
              <p>{data.projectDetail}</p>
              <div className="latest-project-goal">
                <ProgressBar now={85} variant="success" />
                <p>
                  {data.processPercentage}
                  <span>Төслийн зорилго ₮{data.projectGoal}</span>
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6 latest-project-left">
              <img
                src={data.projectImg}
                alt="project-img"
                className="img-fluid"
              />
            </div>
            <div className="col-6 latest-project-right-author">
              <h5>{data.projectTitle}</h5>
              <p>{data.projectDetail}</p>
              <div className="latest-project-goal">
                <ProgressBar now={85} variant="success" />
                <p>
                  {data.processPercentage}
                  <span>Төслийн зорилго ₮{data.projectGoal}</span>
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6 latest-project-left">
              <img
                src={data.projectImg}
                alt="project-img"
                className="img-fluid"
              />
            </div>
            <div className="col-6 latest-project-right-author">
              <h5>{data.projectTitle}</h5>
              <p>{data.projectDetail}</p>
              <div className="latest-project-goal">
                <ProgressBar now={85} variant="success" />
                <p>
                  {data.processPercentage}
                  <span>Төслийн зорилго ₮{data.projectGoal}</span>
                </p>
              </div>
            </div>
          </div>
        </div>

        <div className="display-lg-row">
          <div className="row">
            <div className="col-6 latest-project-left">
              <img
                src={data.projectImg}
                alt="project-img"
                className="img-fluid"
              />
            </div>
            <div className="col-6 latest-project-right-author">
              <h5>{data.projectTitle}</h5>
              <p>{data.projectDetail}</p>
              <div className="latest-project-goal">
                <ProgressBar now={85} variant="success" />
                <p>
                  {data.processPercentage}
                  <span>Төслийн зорилго ₮{data.projectGoal}</span>
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6 latest-project-left">
              <img
                src={data.projectImg}
                alt="project-img"
                className="img-fluid"
              />
            </div>
            <div className="col-6 latest-project-right-author">
              <h5>{data.projectTitle}</h5>
              <p>{data.projectDetail}</p>
              <div className="latest-project-goal">
                <ProgressBar now={85} variant="success" />
                <p>
                  {data.processPercentage}
                  <span>Төслийн зорилго ₮{data.projectGoal}</span>
                </p>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-6 latest-project-left">
              <img
                src={data.projectImg}
                alt="project-img"
                className="img-fluid"
              />
            </div>
            <div className="col-6 latest-project-right-author">
              <h5>{data.projectTitle}</h5>
              <p>{data.projectDetail}</p>
              <div className="latest-project-goal">
                <ProgressBar now={85} variant="success" />
                <p>
                  {data.processPercentage}
                  <span>Төслийн зорилго ₮{data.projectGoal}</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="latest-project-footer">
        <p>
          <a href="/projects">
            <span>
              Бүх нийтлэгдсэн төслийг харах
              <HiOutlineArrowNarrowRight />
            </span>
          </a>
        </p>
      </div>
    </div>
>>>>>>> parent of 90c0cb2 (lates revamped)
  );
};

export default Latest;

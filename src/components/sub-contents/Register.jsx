import React, { useEffect, useRef } from "react";
import { FaFacebookF } from "react-icons/fa";
import { BsGoogle } from "react-icons/bs";
import { Icon } from "react-icons-kit";
import { eye } from "react-icons-kit/feather/eye";
import { eyeOff } from "react-icons-kit/feather/eyeOff";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
const Register = () => {
  const name = useRef();
  const email = useRef();
  const password = useRef();
  const confPassword = useRef();
  const [type, setType] = useState("password");
  const [icon, setIcon] = useState(eyeOff);
  const [signUp, setSignUp] = useState(false);
  const localSignUp = localStorage.getItem("signIn");
  useEffect(() => {
    if (localSignUp) {
      setSignUp(true);
    }
  });

  //Navigate
  const navigate = useNavigate();

  // Toggling password hider Icon
  const handleToggle = () => {
    if (type === "password") {
      setIcon(eye);
      setType("text");
    } else {
      setIcon(eyeOff);
      setType("password");
    }
  };

  // Creating account for user and sending user to Logins

  const submitHandler = (e) => {
    e.preventDefault();
    localStorage.getItem("name");
    localStorage.getItem("email");
    localStorage.getItem("password");
    localStorage.getItem("confirmPassword");
    alert("Account created successfully");
    navigate({ pathname: "/login" });
    window.location.reload(true);
  };
  return (
    <div className="Register">
      <div className="row register-row">
        <div className="col col-lg-6 register-left-col">
          <div className="register-header">
            <h3>
              Хүсэл мөрөөдлөө <br /> бусадтай хуваалц
            </h3>
            <img src="/images/green-step.svg" alt="green-img" />
          </div>
          <div className="register-btns">
            <div className="welcome-btn">
              <button type="button">
                <img src="/images/wave-emoji.svg" alt="wave-emoji" />
              </button>
              <div className="welcome-btn-imgs">
                <img src="/images/profile-pic.svg" alt="profile-img" />
                <img src="/images/profile-pic.svg" alt="profile-img" />
                <img src="/images/profile-pic.svg" alt="profile-img" />
              </div>
            </div>
            <div className="three-dot">
              <button type="submit">
                <img src="/images/3dot.svg" alt="..." />
              </button>
            </div>
          </div>

          <div className="register-footer">
            <p>
              Бүртгэлтэй хэрэглэгч бол <a href="/login">Нэвтрэх</a>
            </p>
          </div>
        </div>
        <div className="col col-lg-6 register-right-col">
          {/* When User click sign in button submitHandler function will activate  */}
          <form className="register-form" onSubmit={submitHandler}>
            <h2>Бүртгүүлэх</h2>
            <label htmlFor="username">
              <span className="register-label-span">Нэвтрэх нэр (*)</span>
              <input
                ref={name}
                type="text"
                name="username"
                placeholder="User name"
                required
              />
            </label>
            <label htmlFor="email">
              <span className="register-label-span">Цахим хаяг (*)</span>
              <input
                ref={email}
                type="email"
                name="email"
                id=""
                placeholder="Email"
                required
              />
            </label>
            <div className="register-password-labels">
              <label htmlFor="password">
                <span>Нууц үг (*)</span>
                <div className="register-password-div">
                  <input
                    ref={password}
                    type={type}
                    name="password"
                    placeholder="Password"
                    required
                  />
                  <span>
                    <Icon icon={eyeOff} onClick={handleToggle} size={20} />
                  </span>
                </div>
              </label>
              <label htmlFor="conf-password">
                <span>Нууц үг давтах(*)</span>
                <div className="register-confirm-password-div">
                  <input
                    ref={confPassword}
                    type={type}
                    name="conf-password"
                    placeholder="Password"
                    required
                  />
                  <span>
                    <Icon icon={eyeOff} onClick={handleToggle} size={20} />
                  </span>
                </div>
              </label>
            </div>
            <div className="term-cond">
              <input type="checkbox" required />
              <span>Үйлчилгээний нөхцөл зөвшөөрөх</span>
            </div>
            <button type="submit" className="register-btn">
              Бүртгүүлэх
            </button>
          </form>
          <div className="social-media-login">
            <p>Эсвэл эдгээрээр нэвтрэх</p>
            <div className="social-media-login-btn">
              <button type="button">
                <span>
                  <BsGoogle size={15} id="google-icon" />
                  <p>Google Account</p>
                </span>
              </button>
              <button type="button">
                <span>
                  <FaFacebookF size={15} />
                  <p className="fa-facebook-icon"> Facebook</p>
                </span>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;

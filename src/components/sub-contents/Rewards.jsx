import React from "react";
import { FiArrowRight } from "react-icons/fi";
const Rewards = () => {
  return (
    <div className="Reward">
      <div className="reward-header">
        <h2>
          Fatlesque Fest <br /> Northwest is Brewing in <br /> Seattle
        </h2>
        <div className="reward-header-input">
          <p>
            <span>₮</span>20 000
          </p>
          <button>
            <FiArrowRight />
          </button>
        </div>
      </div>
      <h5>Дэмжих төрөл</h5>
      <div className="reward-body">
        <div className="reward-header-input">
          <p>
            <span>₮</span>20 000 Reward-name
          </p>
          <button>
            <FiArrowRight />
          </button>
        </div>
        <div className="reward-header-input">
          <p>
            <span>₮</span>40 000 Reward-name
          </p>
          <button>
            <FiArrowRight />
          </button>
        </div>
        <div className="reward-header-input">
          <p>
            <span>₮</span>60 000 Reward-name
          </p>
          <button>
            <FiArrowRight />
          </button>
        </div>
      </div>
    </div>
  );
};

export default Rewards;

import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Font } from "../FontAwesome/FontAwesome";
import "bootstrap/dist/css/bootstrap.min.css";
import { Offcanvas } from "react-bootstrap";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
const Header = () => {
  const navigate = useNavigate();
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const addProjectBtn = () => {
    navigate({ pathname: "/add-projects" });
  };
  return (
    <div className="Header">
      <div className="logo-div">
        <a href="/">
          <img src="/images/logo.svg" alt="logo" />
          <span>Seedshare</span>
        </a>
      </div>
      <div className="header-search">
        <FontAwesomeIcon icon="fa-solid fa-magnifying-glass" />
        <input type="search" placeholder="Төсөл, мэдээ, хайлт хийх" />
      </div>
      <div className="header-btn-div">
        <a href="/login">Нэвтрэх</a>
        <button type="submit" onClick={addProjectBtn}>
          <img src="/images/btn-img.svg" alt="fire" />
          <span>Төсөл байршуулах</span>
        </button>
      </div>
      <div className="header-dropdown">
        <FontAwesomeIcon icon="fa-solid fa-bars" onClick={handleShow} />
        <Offcanvas show={show} onHide={handleClose}>
          <Offcanvas.Header closeButton></Offcanvas.Header>
          <Offcanvas.Body>
            <ul>
              <li>
                <a href="/login">Нэвтрэх</a>
              </li>
              <li>
                <a href="/register">Бүртгүүлэх</a>
              </li>
              <li>
                <a href="/add-projects">Төсөл байршуулах</a>
              </li>
              <li>
                <a href="/veiw-projects">Төслүүд үзэх</a>
              </li>
            </ul>
          </Offcanvas.Body>
        </Offcanvas>
      </div>
    </div>
  );
};

export default Header;
